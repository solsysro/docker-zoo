# DOCKER ZOO-PROJECT 1.7.0 with ESA SNAP 7.0

ARG SNAP_NAME='esa-snap_sentinel_unix_7_0'
ARG SNAP_VERSION='7.0'

FROM ubuntu:18.04

# docker run -it -p 8282:80 -v /Users/silviu/services/spero/services/wps-output:/mnt/wps-output -v /Users/silviu/services/spero/services/wps-services:/opt/wps-services -v /home/silviu/Documents/Development/SPERO/dhus-cli:/opt/dhus-cli --cpus=6 --memory=12g --entrypoint "bash" 2cfd6c18a5f2
#
#

ARG SNAP_NAME
ARG SNAP_VERSION

MAINTAINER Silviu Panica <silviu@solsys.ro>
ENV DEBIAN_FRONTEND noninteractive
RUN apt -y update && apt -y upgrade && \
	apt -y install software-properties-common apache2 crudini openjdk-11-jre less nano wget python-lxml python-shapely python-configparser bc && \
	add-apt-repository ppa:osgeolive/release-13.0 && \
	apt -y update 
RUN apt -y install zoo-api zoo-kernel python-gdal && \
	a2enmod cgi && \
	a2enmod rewrite && \
	a2enmod headers && \
	ln -s  /usr/lib/jvm/java-11-openjdk-amd64/lib/server/libjvm.so /usr/lib/
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9 && \
	add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu bionic-cran35/' && \
	apt -y install r-base libudunits2-dev libgdal-dev && \
	echo 'install.packages("udunits2", repos="https://cran.rstudio.com", configure.args="--with-udunits2-include=/usr/include")' | R --no-save && \
	echo 'install.packages("raster", repos="https://cran.rstudio.com", dependencies=TRUE)' | R --no-save && \
	echo 'install.packages("BH", repos="https://cran.rstudio.com", dependencies=TRUE)' | R --no-save && \
	wget https://cran.r-project.org/src/contrib/Archive/velox/velox_0.2.0.tar.gz -O /tmp/velox_0.2.0.tar.gz && \
	echo 'install.packages("/tmp/velox_0.2.0.tar.gz", dependencies=TRUE, type="source")' | R --no-save

RUN apt -y install saga jq php gdal-bin
# install ESA SNAP
RUN wget http://step.esa.int/downloads/${SNAP_VERSION}/installers/${SNAP_NAME}.sh -O /tmp/${SNAP_NAME}.sh
#COPY /files/${SNAP_NAME}.sh /tmp/
RUN	bash /tmp/${SNAP_NAME}.sh -q -dir /opt/${SNAP_NAME} && rm -f /tmp/${SNAP_NAME}.sh
RUN mkdir /mnt/wps-output; mkdir /opt/wps-services
ENV PATH="${PATH}:/opt/${SNAP_NAME}/bin"

RUN apt -y install unzip git python3.7 python3-distutils python-setuptools
RUN cd /opt && git clone git://github.com/kennethreitz/requests.git && cd requests && python2.7 setup.py install
RUN cd /tmp && wget https://bootstrap.pypa.io/get-pip.py -O get-pip.py; python3.7 get-pip.py; rm -f get-pip.py
RUN cd /opt && git clone https://bitbucket.org/solsysro/dhus-cli 
RUN cd /opt/dhus-cli && pip3.7 install -r requirements.txt

RUN mkdir /root/.ssh && \
	chmod 0700 /root/.ssh && \
	ssh-keyscan bitbucket.org > /root/.ssh/known_hosts && \
	echo "Host github.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config && \
	echo "Host bitbucket.com\n\tStrictHostKeyChecking no\n" >> /root/.ssh/config
ADD files/id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa

ADD files/spero-ca.crt /usr/local/share/ca-certificates/
RUN update-ca-certificates

EXPOSE 80
COPY scripts/entrypoint.sh /entrypoint.sh
COPY scripts/deploy_processes.sh /deploy_processes.sh
RUN chmod 755 /deploy_processes.sh
RUN chmod 755 /entrypoint.sh
ENTRYPOINT /entrypoint.sh
