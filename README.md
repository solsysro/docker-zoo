# Zoo Project Docker Image

This is an unofficial docker image for the Zoo Project WPS Server. Currently this image is based on Zoo 1.7.0.

*"ZOO-Project is a WPS (Web Processing Service) implementation written in C, Python and JavaScript. It is an open source platform which implements the WPS 1.0.0 and WPS 2.0.0 standards edited by the Open Geospatial Consortium (OGC)."*
 For more information regarding Zoo please see the project homepage at: http://zoo-project.org/

In order to run a Zoo container you can:

```
docker run --name zoo-server-instance -d silviu001/zoo-project:1.7.0
```

The container can be further customised by the following environment variables:

 - `ZOO_SERVER_ADDRESS`: URL to the ZOO-Kernel instance; The default value is `http://localhost/cgi-bin/zoo_loader.cgi`
 - `ZOO_TMP_URL`: URL to access the temporary files directory; The default value is `http://localhost/temp/`
 - `ZOO_CORS`: Define if the ZOO-Kernel should support Cross-Origin Resource Sharing. If this parameter is not defined, then the ZOO-Kernel won’t support CORS
 - `ZOO_CORS_ALLOW_ORIGIN`: URI allowed to access the resource. The default values is `*`

