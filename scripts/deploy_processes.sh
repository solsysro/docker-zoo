#!/bin/bash

set -euo pipefail

if [ ! -d ${WPS_PROCESSES_HOME} ];then
    echo "[--] Creating ${WPS_PROCESSES_HOME} and cloning the sources ..."
    mkdir -p ${WPS_PROCESSES_HOME}
    cd ${WPS_PROCESSES_HOME}
    git clone git@bitbucket.org:solsysro/docker-services.git .
fi

cd ${WPS_PROCESSES_HOME}
if [ ! -d .git ];then
    echo "Cloning the repository for the first time ..."
    git clone git@bitbucket.org:solsysro/docker-services.git .
else
    echo "Pulling changes from the main repository ..."
    git pull
fi 
for i in `ls .`;do
    if [ -d ${i} ];then
        if [ ! -f ${i}/deploy.sh ];then
            continue
        fi
        cd ${i}
        bash deploy.sh
        cd ..
    fi
done
